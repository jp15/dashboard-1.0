import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PrincipalComponent } from './principal/principal.component';
import { ProductividadComponent } from './productividad/productividad.component';



const routes: Routes = [
  {path:'', component:PrincipalComponent},
  {path:'productividad', component:ProductividadComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
