import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NivelesChartComponent } from './niveles-chart.component';

describe('NivelesChartComponent', () => {
  let component: NivelesChartComponent;
  let fixture: ComponentFixture<NivelesChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NivelesChartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NivelesChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
